#!/bin/bash

#Run deploy_ec2_and_ping module
cd deploy_ec2_and_ping
terraform init
terraform apply

#Run record_ping_output module
cd ../record_ping_output
terraform init
terraform apply
