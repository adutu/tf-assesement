locals {
  instance_passwords = {
    for instance_name, instance_config in var.configuration : 
    instance_name => random_password.generate_passwords[instance_name].result
  }
}

resource "random_password" "generate_passwords" {
  for_each = var.configuration

  length           = 16
  special          = true
  override_special = "_%@"
}

/*resource "aws_secretsmanager_secret" "ec2_passwords" {
  for_each = local.instance_passwords

  name = "EC222-${each.key}-RootPassword"
}

resource "aws_secretsmanager_secret_version" "ec2_password_versions" {
  for_each = aws_secretsmanager_secret.ec2_passwords

  secret_id     = each.value.id
  secret_string = local.instance_passwords[each.key]
}*/

resource "aws_key_pair" "my_key_pair" {
  key_name   = "my-key"
  public_key = file("~/.ssh/id_rsa.pub")  
}

resource "aws_instance" "ec2" {
  for_each = var.configuration

  ami           = each.value.ami
  instance_type = each.value.instance_type
  subnet_id     = "subnet-03fca5075a5d7aa6b"
  vpc_security_group_ids = ["sg-078c7d4136c07e51d"]
  key_name      = aws_key_pair.my_key_pair.key_name

  user_data = <<-EOF
              #!/bin/bash
              echo "root:${local.instance_passwords[each.key]}" | chpasswd
              EOF
  tags = {
    Name = each.key
  }

}

resource "null_resource" "ping_instances" {
  depends_on = [aws_instance.ec2] // 
  count = length(var.configuration)

  provisioner "local-exec" {
      
      command = <<EOT
                  sleep 60
                  ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa ec2-user@${aws_instance.ec2[keys(var.configuration)[count.index]].public_ip} "echo '${keys(var.configuration)[count.index]}' pinging ${keys(var.configuration)[(count.index + 1) % length(keys(var.configuration))]} >> /tmp/ping_output_${count.index}.txt; ping -c 3 ${aws_instance.ec2[keys(var.configuration)[(count.index + 1) % length(keys(var.configuration))]].public_ip}; [[ $? -eq 0 ]] && echo "pass" >> /tmp/ping_output_${count.index}.txt || echo "failed" >> /tmp/ping_output_${count.index}.txt"
                  scp -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa ec2-user@${aws_instance.ec2[keys(var.configuration)[count.index]].public_ip}:/tmp/ping_output_${count.index}.txt /tmp/
                  cat /tmp/ping_output_${count.index}.txt >> /tmp/ping_results.txt
                  rm /tmp/ping_output_${count.index}.txt
                EOT
  }
}

