output "instance_public_ips" {
  value = {
    for instance_name, instance in aws_instance.ec2 : instance_name => instance.public_ip
  }
  sensitive = true
}
