provider "aws" {
  region                  = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket         = "tf-assesement-state"
    key            = "statefile-deploy-ec2-and-ping.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

