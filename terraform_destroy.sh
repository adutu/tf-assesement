#!/bin/bash

#Destroy deploy_ec2_and_ping module
cd deploy_ec2_and_ping
echo "yes" | terraform destroy

#Destroy record_ping_output module
cd ../record_ping_output
echo "yes" | terraform destroy
