provider "aws" {
  region                  = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket         = "tf-assesement-state"
    key            = "statefile-record-ping.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

