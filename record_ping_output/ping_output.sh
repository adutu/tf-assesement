#!/bin/bash
# File path
file_path="/tmp/ping_results.txt"

# Function to format the output as JSON
function generate_json {
  local json="{"
  local key
  while IFS= read -r line; do
    if [[ $line =~ "pinging" ]]; then
      key=$(echo "$line")
      read -r pass_fail_line
      value=$pass_fail_line
      json+="\"$key\":\"$value\","
    fi
  done < "$file_path"
  # Remove the trailing comma and close JSON object
  json="${json%,*}}"
  echo "$json"
}

# Generate JSON output
generate_json

# Empty file
cat /dev/null > /tmp/ping_results.txt
